SHELL := /bin/bash
PATH := bin:${PATH}

dev: clean buildc

# stack
build: bin
	stack build
buildv: bin
	stack build --verbose
buildc: bin
	stack build --file-watch
clean:; stack clean
clobber:; rm -rf .stack-work

LTS = $(shell egrep '^resolver:' stack.yaml | awk '{print $$2}')
bin:; ln -fs .stack-work/install/x86_64-linux/${LTS}/7.10.2/bin

# docker
DOCKER_IDS ?= $(shell docker ps --all --quiet)
docker-ps:; docker ps
docker-ls:; docker ps --all
docker-stop:; [ -z "${DOCKER_IDS}" ] || docker stop ${DOCKER_IDS}
docker-clean:; [ -z "${DOCKER_IDS}" ] || docker rm --force ${DOCKER_IDS}
docker-clobber: docker-stop docker-clean
# e.g.: make docker-sh id=d1defa6a9edb
docker-sh:; docker exec --interactive=true --tty=true ${id} /bin/bash

daemon-start:; for i in docker.service docker.socket; do sudo systemctl start $$i; done
daemon-stop:; for i in docker.service docker.socket; do sudo systemctl stop $$i; done
daemon-restart: daemon-stop daemon-start

init:
	stack setup --reinstall
	@if [ ! -d "lib/docker" ]; then \
		git submodule add git@gitlab.com:metaml/docker lib/docker; \
		git submodule init; \
		echo "git submodule lib/docker added; please commit"; \
	fi

.PHONY: dev build buildc buildv clean clobber init bin \
	docker-ps docker-clean docker-stop \
	daemon-start daemon-stop daemon-restart
